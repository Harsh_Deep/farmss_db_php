<?php

    include 'includes/functions.php';


    //data is to be fetched

    if(isset($_POST['id']) && isset($_POST['conc'])){

        //taking concentration 
        $conc = $_POST['conc'];

        //taking id 
        $farmer_id = $_POST['id'];


        if(hasFarmerId($farmer_id)){

            //checking whether farmer has added test details
            $tests = mysqli_fetch_array(hasTest($farmer_id));
            if($tests['electrical_conductivity']){
            
            $response['success'] = 0;
            $response['msg'] = "This farmer has already this test";

            }else{

                try{

                        $query = updateConductivity($farmer_id, $conc);

                        //adding json data into an array
                        $response['success'] = 1;
                        $response['msg'] = "Data has been Successfully updated. :)";


                    }catch(Exception $e){
                        $response['success'] = 0;
                        $response['msg'] = $e->getMessage();

                    }

            }
        }else{

                try{

                    $query = addConductivity($farmer_id, $conc);

                    //adding json data into an array
                    $response['success'] = 1;
                    $response['msg'] = "Data has been Successfully uploaded. :)";


                }catch(Exception $e){
                    $response['success'] = 0;
                    $response['msg'] = $e->getMessage();

                }
            }


     

    }else{

        $response['sucess'] = 0;
        $response['msg'] = "Missing something. :(";

    }

        echo json_encode($response);


?>