<?php

    include 'includes/functions.php';


    //data is to be fetched

    if(isset($_POST['id']) && isset($_POST['conc']) && isset($_POST['image'])){

        //taking concentration 
        $conc = $_POST['conc'];

        //taking id 
        $farmer_id = $_POST['id'];

        //taking image
        $image = $_POST['image'];


        //address of the image where being uploaded
        $imgUrl = 'farmssUploads/potassium_'. getFileName() .'.jpg';

        $imgUrlShow = 'http://homephp.hol.es/'. $imgUrl;

        //checkin whether farmer has this test already


        if(hasFarmerId($farmer_id)){

            //checking whether farmer has added test details
            $tests = mysqli_fetch_array(hasTest($farmer_id));
            if($tests['potassium_conc'] && $tests['potassium_img']){
            
            $response['success'] = 0;
            $response['msg'] = "This farmer has already this test";

            }else{

                try{

                        //file will be saved after running this 
                        file_put_contents($imgUrl, base64_decode($image));

                        $query = updatePotassium($farmer_id, $conc, $imgUrlShow);

                        //adding json data into an array
                        $response['success'] = 1;
                        $response['imgUrl'] = $imgUrlShow;
                        $response['msg'] = "Data has been Successfully updated. :)";


                    }catch(Exception $e){
                        $response['success'] = 0;
                        $response['msg'] = $e->getMessage();

                    }

            }
        }else{

                try{

                    //file will be saved after running this 
                    file_put_contents($imgUrl, base64_decode($image));

                    $query = addPotassium($farmer_id, $conc, $imgUrlShow);

                    //adding json data into an array
                    $response['success'] = 1;
                    $response['imgUrl'] = $imgUrlShow;
                    $response['msg'] = "Data has been Successfully uploaded. :)";


                }catch(Exception $e){
                    $response['success'] = 0;
                    $response['msg'] = $e->getMessage();

                }
            }


     

    }else{

        $response['sucess'] = 0;
        $response['msg'] = "Missing something. :(";

    }

        echo json_encode($response);


?>