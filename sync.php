<?php

	//connection file
	include 'includes/functions.php';

	//data to be fetched
    $jsonData = file_get_contents("php://input");
if($jsonData){


	//removing special slashes from that json data

	//checking whether it contains slashes or not
	if(get_magic_quotes_gpc()){
		$jsonData = stripslashes($jsonData);
	}

	//now decode that json data into an array
	$data = json_decode($jsonData, true);

	//loop through json data so that we can add that data into sql database
	foreach ($data['farmer'] as $item) {

		//extracting data from that json
        $name = $item['name'];
        $mobile = $item['mobile'];
        $state = $item['state'];
        $district = $item['district'];
        $tehsil = $item['tehsil'];
        $village = $item['village'];
        $pincode = $item['pincode'];
        $plot_size = $item['plot_size'];
        $crop = $item['crop'];
        $lat = $item['lat'];
        $long = $item['long'];

        //creating unique id for farmer
        $nameArray = explode(' ', $name);
        $namelen = count($nameArray);

        //first two letters of first name and last name
        $uuid = strtoupper($nameArray[0][0].$nameArray[$namelen-1][0]);

        //adding phone last four digits
        $uuid .= substr($mobile, -4);

        // adding test detail
        $tests = userTests($mobile);

        //padding 0 left so that => 01 not 1
        $uuid .= "_".str_pad($tests, 2, "0", STR_PAD_LEFT);

        //adding this data into database
        $added = addFarmer($uuid, $name, $mobile, $state, $district, $tehsil, $village, $pincode, $plot_size, $crop, $lat, $long);
        }



   //checking whether data got added or not
        if($added){
        	//if yes
        	$response['success'] = 1;
        	$response['Message'] = count($data["farmer"])." record(s) successfully added.. :)";

        	//json data output
        	echo json_encode($response);

        }else{
        	//if not successful
        	$response['success'] = 0;
        	$response['Message'] = "Couldn't add records.. :(";
        }
}else{
    $response['success'] = 0;
    $response['Message'] = "Something is missing.. :(";
    echo json_encode($response);
}


?>