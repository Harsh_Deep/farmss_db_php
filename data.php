<html>
<head>
<title>Farmers</title>
<style>
table {
    width:100%;
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;
}
table#t01 tr:nth-child(even) {
    background-color: #eee;
}
table#t01 tr:nth-child(odd) {
   background-color:#eee;
}
table#t01 th {
    background-color: green;
    color: white;
}
</style>
</head>
<body>

<h4 style="text-align:center">All Farmers: </h4>

<table id="t01">
  <tr>
    <th>Sr.No.</th>
    <th>Id</th>
    <th>Name</th>
    <th>Mobile</th>
    <th>State</th>
    <th>District</th>
    <th>Tehsil</th>
    <th>Village</th>
    <th>Pincode</th>
    <th>Plot-Size</th>
    <th>Crop</th>
    <th>Latitude</th>
    <th>Longitude</th>
    <th>Added On</th>
  </tr>


<?php

    include 'includes/functions.php';

        //getting all farmers
        $users = getAllFarmers();

        $counter = 1;

       //accessing all details of farmers
        while($row = mysqli_fetch_array($users)){

            echo "<tr><td>".$counter."</td>";
            echo "<td>".$row['unique_id']."</td>";
            echo "<td>".$row['name']."</td>";
            echo "<td>".$row['mobile']."</td>";
            echo "<td>".$row['state']."</td>";
            echo "<td>".$row['district']."</td>";
            echo "<td>".$row['tehsil']."</td>";
            echo "<td>".$row['village']."</td>";
            echo "<td>".$row['pincode']."</td>";
            echo "<td>".$row['plot_size']."</td>";
            echo "<td>".$row['crop']."</td>";
            echo "<td>".$row['latitude']."</td>";
            echo "<td>".$row['longitude']."</td>";
            echo "<td>".$row['registered_on']."</td></tr>";

            $counter ++;


        }





?>

</table>
</body>
</html>