<?php

    //accessing connection file
    include_once('conn.php');

    //storing user in the database
    function addFarmer($uuid, $name, $mobile, $state, $district, $tehsil, $village, $pincode, $plot_size, $crop, $lat, $long){
        //declaring this as global
        global $conn;

        //storing data into the database
        $insert_data = "INSERT INTO farmers(unique_id, name, mobile, state, district, tehsil, village, pincode, plot_size, crop, latitude, longitude) VALUES ('$uuid', '$name', '$mobile', '$state', '$district', '$tehsil', '$village', '$pincode', '$plot_size', '$crop', '$lat', '$long')";
        $result = mysqli_query($conn, $insert_data);


        //checking whether the data got stored
        if($result){

            //if successful in storing data
            return true;
        }else {

            //if not successful in storing data
            return false;
        }

    }


    //checking whether the farmer has tests
    function isTestIdExisted($id){

        global $conn;

        $query = "SELECT * FROM tests WHERE farmer_id = '$id'";

        $result = mysqli_query($conn, $query);

        if(mysqli_num_rows($result) > 0){
            return true;

        }else{
            return false;
        }
    }

    // checking if user exists but not test
    function userButID($id){

        global $conn;

        $query = "SELECT * FROM farmers WHERE unique_id = '$id'";

        $result = mysqli_query($conn, $query);

        if(mysqli_num_rows($result) > 0){
            return true;

        }else{
            return false;
        }
    }

    //getting all farmers details
    function getAllFarmers(){

        //accessing connection file
        global $conn;

        //query for that
        $query = "SELECT * FROM farmers";

        //result of that query
        $result = mysqli_query($conn, $query);

        return $result;
    }

    //return file name that is to be uploaded
    function getFileName(){

        global $conn;

        $query = "SELECT id FROM tests ORDER BY id ASC";
        $result = mysqli_query($conn, $query);

        $id = 0;

        while($row = mysqli_fetch_array($result)){
            $id = $row['id'];
        }

        return $id;
    }


    //checking whether the farmer has added test or not
    function hasTest($id){

        global $conn;

        $query = "SELECT * FROM tests WHERE farmer_id = '$id'";
        $query = mysqli_query($conn, $query);

        return $query;


    }

    //checking whether it has farmer_id added or not
    function hasFarmerId($id){

        global $conn;

        $query = mysqli_query($conn, "SELECT * FROM tests WHERE farmer_id = '$id'");

        return mysqli_num_rows($query);
    }


    //updating NitrateTest details
    function updateNitrate($id, $conc, $img){

        global $conn;

        $query = "UPDATE tests SET nitrate_conc='$conc', nitrate_img='$img'
                    WHERE farmer_id = '$id' ";
        $query = mysqli_query($conn, $query);

        return $query;


    }



    //adding NitrateTest details
    function addNitrate($id, $conc, $img){

            global $conn;

            //inserting data into database
            $query = "INSERT INTO tests(farmer_id, nitrate_conc, nitrate_img)
                        VALUES('$id', '$conc', '$img')";
            $query = mysqli_query($conn, $query);

            return $query;

    }

    //updating PotassiumTest details
    function updatePotassium($id, $conc, $img){

        global $conn;

        $query = "UPDATE tests SET potassium_conc='$conc', potassium_img='$img'
                    WHERE farmer_id = '$id'";
        $query = mysqli_query($conn, $query);

        return $query;


    }



    //adding PotassiumTest details
    function addPotassium($id, $conc, $img){

            global $conn;

            //inserting data into database
            $query = "INSERT INTO tests(farmer_id, potassium_conc, potassium_img)
                        VALUES('$id', '$conc', '$img')";
            $query = mysqli_query($conn, $query);

            return $query;

    }

    //updating PhosphateTest details
    function updatePhosphate($id, $conc, $img){

        global $conn;

        $query = "UPDATE tests SET phosphate_conc='$conc', phosphate_img='$img'
                    WHERE farmer_id = '$id'";
        $query = mysqli_query($conn, $query);

        return $query;


    }



    //adding PhosphateTest details
    function addPhosphate($id, $conc, $img){

            global $conn;

            //inserting data into database
            $query = "INSERT INTO tests(farmer_id, phosphate_conc, phosphate_img)
                        VALUES('$id', '$conc', '$img')";
            $query = mysqli_query($conn, $query);

            return $query;

    }

    //updating PhTest details
    function updatePh($id, $conc){

        global $conn;

        $query = "UPDATE tests SET ph_conc='$conc' WHERE farmer_id = '$id'";
        $query = mysqli_query($conn, $query);

        return $query;


    }



    //adding PhTest details
    function addPh($id, $conc){

            global $conn;

            //inserting data into database
            $query = "INSERT INTO tests(farmer_id, ph_conc)
                        VALUES('$id', '$conc')";
            $query = mysqli_query($conn, $query);

            return $query;

    }

    //updating electricalTest details
    function updateConductivity($id, $cond){

        global $conn;

        $query = "UPDATE tests SET electrical_conductivity ='$cond'
                        WHERE farmer_id = '$id'";
        $query = mysqli_query($conn, $query);

        return $query;


    }



    //adding electricalTest details
    function addConductivity($id, $cond){

            global $conn;

            //inserting data into database
            $query = "INSERT INTO tests(farmer_id, electrical_conductivity)
                        VALUES('$id', '$cond')";
            $query = mysqli_query($conn, $query);

            return $query;

    }

    //getting all test details
    function getAllTests(){

        //accessing connection file
        global $conn;

        //query for that
        $query = "SELECT * FROM tests";

        //result of that query
        $result = mysqli_query($conn, $query);

        return $result;
    }

    //get all concentrations by Id
    function getAllTestsById($id){

        global $conn;

        //query for getting all details
        $query = "SELECT * FROM tests WHERE farmer_id = '$id'";
        $result = mysqli_query($conn, $query);

        //returning all tests query
        return $result;
    }

    //function to see user has how many tests
    function userTests($mob){

        global $conn;

        //checking in database
        $query = "SELECT COUNT(*) from tests WHERE mobile = $mobile";
        $number = mysqli_query($conn, $query);

        //returning this number
        return $number+1;
    }

     //checking whether it has farmer_id added or not
    function existUser($id){

        global $conn;

        $query = mysqli_query($conn, "SELECT * FROM tests WHERE farmer_id = '$id'");

        return $query;
    }

?>