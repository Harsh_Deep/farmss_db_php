<html>
<head>
<title>Tests</title>
<style>
table {
    width:100%;
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;
}
table#t01 tr:nth-child(even) {
    background-color: #eee;
}
table#t01 tr:nth-child(odd) {
   background-color:#eee;
}
table#t01 th {
    background-color: green;
    color: white;
}
</style>
</head>
<body>

<h4 style="text-align:center">All Tests: </h4>

<table id="t01">
  <tr>
    <th>Sr.No.</th>
    <th>Id</th>
    <th>Nitrate Conc.</th> 
    <th>Nitrate Image</th>
    <th>Potassium Conc.</th>
    <th>Potassium Image</th> 
    <th>Phosphate Conc.</th>
    <th>Phosphate Image</th>
    <th>PH Conc.</th> 
    <th>Electrical Conductivity</th>
  </tr>


<?php                                
    
    include 'includes/functions.php';

        //getting all farmers
        $users = getAllTests();

        $counter = 1;

       //accessing all details of farmers
        while($row = mysqli_fetch_array($users)){

            echo "<tr><td>".$counter."</td>";
            echo "<td>".$row['farmer_id']."</td>";
            echo "<td>".$row['nitrate_conc']."</td>";
            echo "<td><a href='".$row['nitrate_img']."'>IMG</a></td>";
            echo "<td>".$row['potassium_conc']."</td>";
            echo "<td><a href='".$row['potassium_img']."'>IMG</a></td>";
            echo "<td>".$row['phosphate_conc']."</td>";
            echo "<td><a href='".$row['phosphate_img']."'>IMG</a></td>";
            echo "<td>".$row['ph_conc']."</td>";
            echo "<td>".$row['electrical_conductivity']."</td></tr>";
            $counter ++;


        }





?>

</table>
</body>
</html>