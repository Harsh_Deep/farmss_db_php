<?php
	
	//accessing connection file
	require_once 'includes/functions.php';

	//checking data has been posted by the farmer
	if(isset($_POST['name']) && isset($_POST['mobile']) && isset($_POST['state']) && isset($_POST['district']) && isset($_POST['tehsil']) && isset($_POST['village']) && isset($_POST['pincode']) && isset($_POST['plot_size']) && isset($_POST['crop'])){



		// storing entered data into varibles
		$name     = $_POST['name'];
		$mobile   = $_POST['mobile'];
		$state    = $_POST['state'];
		$district = $_POST['district'];
		$tehsil   = $_POST['tehsil'];
		$village  = $_POST['village'];
		$pincode  = $_POST['pincode'];
		$plotsize = $_POST['plot_size'];
		$crop     = $_POST['crop']; 

  
		//checking whether farmer is already registered
		if(isFarmerExisted($mobile)){
			$response['success'] = 0;
			$response['error_msg'] = 'Farmer is already registered';
			echo json_encode($response);
		}else{
			
			// received all details of the username
			$user = addFarmer($name, $mobile, $state, $district, $tehsil, $village, $pincode, $plotsize, $crop);

			//checking that retreived data is Ok
			if($user){
				
				//packing json data
				$response['status']           = "REGISTERED !!";
				$response['success']          = 1;
				$response['uid']              = $user['unique_id'];
				$response['user']['name']     = $user['name'];
				$response['user']['mobile']   = $user['mobile'];
				$response['user']['state']    = $user['state'];
				$response['user']['district'] = $user['district'];
				$response['user']['tehsil']   = $user['tehsil'];
				$response['user']['village']  = $user['village'];
				$response['user']['pincode']  = $user['pincode'];
				$response['user']['plotSize'] = $user['plot_size'];
				$response['user']['crop']     = $user['crop'];

				//sending json data now
				echo json_encode($response);
			}else{

				$response['success'] = 0;
				$response['error_msg'] = "Something unknown is here. checking...";
				echo json_encode($response);
			}
		}
	}else{
		$response['success'] = 0;
		$response['error_msg'] = "Something is missing..";
		echo json_encode($response);
	}

?>