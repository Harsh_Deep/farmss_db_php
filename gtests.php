<?php

	//accessing all function files
	include_once ('includes/functions.php');

	if(isset($_POST['id'])){

		$farmer_id = $_POST['id'];

		//checking whether this id is in our database
		if(isTestIdExisted($farmer_id)){

			//if yes
			$test = mysqli_fetch_array(getAllTestsById($farmer_id));

			$response['success'] = 1;
			$response['Nitrate_Conc'] = $test['nitrate_conc'];
			$response['Potassium_Conc'] = $test['potassium_conc'];
			$response['Phosphate_Conc'] = $test['phosphate_conc'];
			$response['Ph_Conc'] = $test['ph_conc'];
			$response['Electrical_Condunctivity'] = $test['electrical_conductivity'];

		}else if(userButID($farmer_id)){

			$response['success'] = 1;
			$response['msg'] = 'User exists but no test is there.';
		}else{

            $response['success'] = 0;
            $response['msg'] = 'User is not present';
        }

    }else{
		$response['success'] = 0;
		$response['msg'] = 'missing something.. :(';
	}

	//sending json data
	echo json_encode($response);

?>